#include <X11/X.h>
#include <X11/XKBlib.h>
#include <GL/gl.h>
#include <GL/glx.h>
#include <stdio.h>
#include <string.h>

#define IMAGE_X 20
#define IMAGE_Y 20

void draw_square(void)
{
	glBegin(GL_QUADS);
	glVertex2f(-1.0f, 1.0f);
	glVertex2f(1.0f, 1.0f);
	glVertex2f(1.0f, -1.0f);
	glVertex2f(-1.0f, -1.0f);
	glEnd();
}

int main()
{
	int counter = 0;
	int image_data[IMAGE_Y*IMAGE_X];

	Display *display = XOpenDisplay(NULL);
	Window root = DefaultRootWindow(display);
	XVisualInfo *visual_info = glXChooseVisual(display, 0, (GLint[]){GLX_RGBA, GLX_DEPTH_SIZE, 24, GLX_DOUBLEBUFFER, None});
	XSetWindowAttributes swa = {
		.colormap = XCreateColormap(display, root, visual_info->visual, AllocNone),
		.event_mask = ExposureMask | KeyPressMask
	};
	Window window = XCreateWindow(display, root, 0, 0, 640, 480, 0,
			visual_info->depth, InputOutput, visual_info->visual,
			CWColormap | CWEventMask, &swa);
	Pixmap pixmap = XCreatePixmap(display, window, IMAGE_X, IMAGE_Y, 24);

	XMapWindow(display, window);
	GLXContext gl_context = glXCreateContext(display, visual_info, NULL, GL_TRUE);
	glXMakeCurrent(display, window, gl_context);

	glXSwapBuffers(display, window);
	GC gc = XCreateGC(display, window, 0, NULL);

	while (1)
	{
		XEvent event;
		if (XCheckWindowEvent(display, window, KeyPressMask, &event))
		{
			if (event.type == KeyPress)
			{
				KeySym sym = XkbKeycodeToKeysym(display, event.xkey.keycode, 0, 0);

				if (sym == XK_n)
				{
					/* both sequences cause the problem */
					/* static const KeySym sequence[] = {XK_g, XK_c, XK_s, XK_r, XK_c, XK_s}; */
					static const KeySym sequence[] = {XK_g, XK_s, XK_c, XK_r, XK_s, XK_c};
					sym = sequence[counter];
					counter = (counter + 1) % (sizeof(sequence)/sizeof(*sequence));
				}

				switch (sym)
				{
				case XK_s: /* (s)wap */
					printf("swapping the buffers\n");
					glXSwapBuffers(display, window);
					break;
				case XK_g: /* (g)reen */
					printf("backbuffer painted green\n");
					glColor3b(0,100,0);
					draw_square();
					break;
				case XK_r: /* (r)ed */
					printf("backbuffer painted red\n");
					glColor3b(100,0,0);
					draw_square();
					break;
				case XK_c: /* (c)opy */
					printf("XCopyArea\n");
					XCopyArea(display, window, pixmap, gc, 0, 0, IMAGE_X, IMAGE_Y, 0, 0);
					XCopyArea(display, pixmap, window, gc, 0, 0, IMAGE_X, IMAGE_Y, 0, 0);
					break;
				case XK_q: /* (q)uit */
					printf("quitting\n");
					return 1;
				}
			}
		}
	}

	return 0;
}
